/*
 Navicat Premium Data Transfer

 Source Server         : lokal
 Source Server Type    : MySQL
 Source Server Version : 100316
 Source Host           : localhost:3306
 Source Schema         : maingames

 Target Server Type    : MySQL
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/07/2021 23:37:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for m_discount
-- ----------------------------
DROP TABLE IF EXISTS `m_discount`;
CREATE TABLE `m_discount`  (
  `discount_id` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `minimal_order` int(10) NOT NULL,
  `discount` int(10) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_by` datetime(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`discount_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for m_inventory
-- ----------------------------
DROP TABLE IF EXISTS `m_inventory`;
CREATE TABLE `m_inventory`  (
  `inventory_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `inventory_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `uom` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_by` datetime(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`inventory_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for m_user
-- ----------------------------
DROP TABLE IF EXISTS `m_user`;
CREATE TABLE `m_user`  (
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `position` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_by` datetime(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for s_initial_stocks
-- ----------------------------
DROP TABLE IF EXISTS `s_initial_stocks`;
CREATE TABLE `s_initial_stocks`  (
  `year` int(10) NOT NULL,
  `month` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `inventory_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `qty` int(10) NOT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_by` datetime(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`year`, `month`, `inventory_id`) USING BTREE,
  INDEX `fk_inventory_id_s_initital_stock`(`inventory_id`) USING BTREE,
  CONSTRAINT `fk_inventory_id_s_initital_stock` FOREIGN KEY (`inventory_id`) REFERENCES `m_inventory` (`inventory_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_production_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_production_detail`;
CREATE TABLE `t_production_detail`  (
  `production_order_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `index` int(10) NOT NULL,
  `inventory_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `qty` int(10) NOT NULL,
  PRIMARY KEY (`production_order_id`, `index`) USING BTREE,
  INDEX `fk_inventory_id_production`(`inventory_id`) USING BTREE,
  CONSTRAINT `fk_inventory_id_production` FOREIGN KEY (`inventory_id`) REFERENCES `m_inventory` (`inventory_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_production_id` FOREIGN KEY (`production_order_id`) REFERENCES `t_production_header` (`production_order_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_production_header
-- ----------------------------
DROP TABLE IF EXISTS `t_production_header`;
CREATE TABLE `t_production_header`  (
  `production_order_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `production_date` date NOT NULL,
  `production_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `notes` int(10) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  `approve` int(1) NOT NULL,
  `discount` int(10) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_by` datetime(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`production_order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_purchase_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_purchase_order_detail`;
CREATE TABLE `t_purchase_order_detail`  (
  `purchase_order_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `index` int(10) NOT NULL,
  `inventory_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `qty` int(10) NOT NULL,
  `discount` int(10) NULL DEFAULT NULL,
  `subtotal` int(10) NOT NULL,
  PRIMARY KEY (`purchase_order_id`, `index`) USING BTREE,
  INDEX `fk_inventory_id_purchase`(`inventory_id`) USING BTREE,
  CONSTRAINT `fk_inventory_id_purchase` FOREIGN KEY (`inventory_id`) REFERENCES `m_inventory` (`inventory_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_purchase_order_id` FOREIGN KEY (`purchase_order_id`) REFERENCES `t_purchase_order_header` (`purchase_order_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_purchase_order_header
-- ----------------------------
DROP TABLE IF EXISTS `t_purchase_order_header`;
CREATE TABLE `t_purchase_order_header`  (
  `purchase_order_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `purchase_date` date NOT NULL,
  `supplier` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `discount` int(10) NULL DEFAULT NULL,
  `grandtotal` int(10) NOT NULL,
  `ppn` int(10) NULL DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `approve` int(1) NOT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_by` datetime(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`purchase_order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sales_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_sales_order_detail`;
CREATE TABLE `t_sales_order_detail`  (
  `sales_order_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `index` int(10) NOT NULL,
  `production_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `qty` int(10) NOT NULL,
  `discount` int(10) NULL DEFAULT NULL,
  `subtotal` int(10) NOT NULL,
  PRIMARY KEY (`sales_order_id`, `index`) USING BTREE,
  INDEX `fk_production_id_sales`(`production_id`) USING BTREE,
  CONSTRAINT `fk_sales_order_id` FOREIGN KEY (`sales_order_id`) REFERENCES `t_sales_order_header` (`sales_order_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_production_id_sales` FOREIGN KEY (`production_id`) REFERENCES `t_production_header` (`production_order_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sales_order_header
-- ----------------------------
DROP TABLE IF EXISTS `t_sales_order_header`;
CREATE TABLE `t_sales_order_header`  (
  `sales_order_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sales_date` date NOT NULL,
  `customer` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `discount` int(10) NULL DEFAULT NULL,
  `grandtotal` int(10) NOT NULL,
  `ppn` int(10) NULL DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `approve` int(1) NOT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_by` datetime(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`sales_order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

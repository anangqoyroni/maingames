<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<form>
		<label>
			<input name="pemain" placeholder="Pemain" type="number" required id="pemain">
		</label>
		<label>
			<input name="jumlah_dadu" placeholder="Jumlah Dadu" type="number" required id="jumlah_dadu">
		</label>
		<button id="submit">Generate</button>
	</form>
	<div id="result"></div>
	<button id='lempar_dadu'>Lempar Dadu</button>
	<br>
	<br>
	<br>
</body>
</html>
<script
src="https://code.jquery.com/jquery-3.6.0.min.js"
integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
crossorigin="anonymous"></script>
<script type="text/javascript">
	$( document ).ready(function() {
		$("#lempar_dadu").hide();
		$("form").show();
		var game = {};
		var jumlah_pemain = 0;
		var jumlah_dadu = 0;
		$("form").submit(function(e){
			e.preventDefault();
			jumlah_pemain = $("#pemain").val();
			jumlah_dadu = $("#jumlah_dadu").val();
			var result = "<br>";
			result += "Pemain = "+jumlah_pemain+", Dadu = "+jumlah_dadu;
			result += "<br><br><hr><hr>";
			$("#result").prepend(result);
			$("form").hide();
			$("#lempar_dadu").show();
			for (var i = 1; i <= jumlah_pemain; i++) {
				var data = {
					dadu:{},
					poin:0
				};
				for (var j = 1; j <= jumlah_dadu; j++) {
					data["dadu"][j]= null;
				}
				game["pemain"+i]=data;
			}
		});
		var lemparan = 1;
		$("#lempar_dadu").click(function(){
			var result = "Giliran <b>"+lemparan+"</b> lempar dadu: <br><br>";
			Object.keys(game).forEach(function(pemain) {
				result += pemain;
				var init_pemain = game[pemain];
				result += " ("+init_pemain["poin"]+") : ";
				var temp_dadu = [];
				Object.keys(init_pemain).forEach(function(dadu) {
					var init_dadu = init_pemain[dadu];
					Object.keys(init_dadu).forEach(function(value_dadu) {
						var result_lempar_dadu = Math.floor(1+ Math.random() * 6);
						if (result_lempar_dadu == 6 ) {
							game[pemain]["poin"] = game[pemain]["poin"]+1;
							delete game[pemain][dadu][value_dadu];
						}
						else{
							game[pemain][dadu][value_dadu] = result_lempar_dadu;
						}
						temp_dadu.push(result_lempar_dadu);
					});
				});
				if (!jQuery.isEmptyObject(game[pemain]["dadu"])) {
					result += temp_dadu.join(",");
				}
				else{
					result += "_ (Berhenti bermain karena tidak memiliki dadu)";
				}
				result += '<br>';
			});

			var available_player = availablePlayer(game);
			Object.keys(game).forEach(function(pemain) {
				var init_pemain = game[pemain];
				var temp_dadu_proses_evaluasi = [];
				var temp_dadu = [];
				var index = available_player.indexOf(pemain);
				if (index == available_player.length - 1) {
					pemain_selanjutnya = available_player[0];
				}
				else{
					pemain_selanjutnya = available_player[index + 1];
				}
				if (!jQuery.isEmptyObject(game[pemain_selanjutnya]["dadu"])) {
					const lastKey = Object.keys(game[pemain_selanjutnya]["dadu"]).pop();
					Object.keys(init_pemain).forEach(function(dadu) {
						var init_dadu = init_pemain[dadu];
						Object.keys(init_dadu).forEach(function(value_dadu) {
							temp_dadu_proses_evaluasi.push(game[pemain][dadu][value_dadu]);
							if (game[pemain][dadu][value_dadu] == 1 ) {
								delete game[pemain][dadu][value_dadu];
								game[pemain_selanjutnya]["dadu"][lastKey+1] =-1;
							}

						});
					});
				}
			});

			result += "<br><br>Setelah evaluasi:<br>";
			var check_game_over = 0;
			var pemenang = {"player": null, "score": 0};
			Object.keys(game).forEach(function(pemain) {
				result += pemain;
				var init_pemain = game[pemain];
				result += " ("+init_pemain["poin"]+") : ";
				var temp_dadu_evaluasi = [];
				var temp_dadu = [];
				if(init_pemain["poin"] > pemenang.score){
					pemenang.player = pemain;
					pemenang.score = init_pemain["poin"];
				}
				Object.keys(init_pemain).forEach(function(dadu) {
					var init_dadu = init_pemain[dadu];
					Object.keys(init_dadu).forEach(function(value_dadu) {
						var nilai = game[pemain][dadu][value_dadu];
						if (game[pemain][dadu][value_dadu] == -1) {
							nilai = 1;
						}
						temp_dadu_evaluasi.push(nilai);
					});
				});
				if (temp_dadu_evaluasi.length == 0) {
					delete game[pemain]["dadu"];
					check_game_over++;
				}
				if (!jQuery.isEmptyObject(game[pemain]["dadu"])) {
					result += temp_dadu_evaluasi.join(",");
				}
				else{
					result += "_ (Berhenti bermain karena tidak memiliki dadu)";
				}
				result += '<br>';
			});

			result += "<br><br><hr><hr>";
			$("#result").append(result);
			if (check_game_over+1 == jumlah_pemain) {
				$("#result").append("<br><br>Permainan berakhir dan dimenangkan oleh <b>"+pemenang.player+"</b> dengan <b>"+pemenang.score+"</b> poin");
				$("#lempar_dadu").hide();
			}
			lemparan++;
		});
	});
	function availablePlayer(game) {
		var arr_pemain_selanjutnya = [];
		Object.keys(game).forEach(function(pemain) {
			if (!jQuery.isEmptyObject(game[pemain]["dadu"])) {
				if(!arr_pemain_selanjutnya.includes(pemain)){
					arr_pemain_selanjutnya.push(pemain);
				}
			}
		});
		return arr_pemain_selanjutnya;
	}
</script>